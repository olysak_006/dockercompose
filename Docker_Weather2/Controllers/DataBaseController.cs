﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Text;
using Microsoft.Extensions.Configuration;
using Npgsql;

namespace Docker_Weather2.Controllers
{
    [ApiController]
    [Route("database/[controller]")]
    public class SQLServerController : ControllerBase
    {
        IConfiguration configuration;
        private readonly ILogger<SQLServerController> _logger;
        public SQLServerController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public SQLServerController(ILogger<SQLServerController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public string Get()
        {
            StringBuilder connectResult = new StringBuilder();
            try
            {
                using (SqlConnection connection = new SqlConnection(configuration.GetSection("SQLServerConnectionString").Value))
                {
                    Console.WriteLine("\nSQLSERVER Query data example:");
                    Console.WriteLine("=========================================\n");

                    connection.Open();

                    String sql = "SELECT name, collation_name FROM sys.databases";

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Console.WriteLine("{0} {1}", reader.GetString(0), reader.GetString(1));
                                connectResult.Append(String.Format("{0} {1}", reader.GetString(0), reader.GetString(1)) + Environment.NewLine);
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.ToString());
                connectResult.Append(e.ToString());
            }
            Console.WriteLine("\nSQLSERVER Done read data.");

            return connectResult.ToString();
        }
    }

    [ApiController]
    [Route("database/[controller]")]
    public class PostgreSQLController : ControllerBase
    {
        private readonly ILogger<PostgreSQLController> _logger;
        IConfiguration configuration;

        public PostgreSQLController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public PostgreSQLController(ILogger<PostgreSQLController> logger)
        {
            _logger = logger;
        }


        [HttpGet]
        public string Get()
        {
            try
            {
                var cs = configuration.GetSection("PostgreSQLConnectionString").Value;//"Host=postgres_image;Username=bloguser;Password=bloguser;Database=blogdb";
                var sql = "SELECT version()";

                Console.WriteLine("\nConnecting to PostgreSQL......");
                Console.WriteLine("\nPostgreSQL query version example:");
                Console.WriteLine("=========================================\n");
                using (var con = new NpgsqlConnection(cs))
                {
                    using (var cmd = new NpgsqlCommand(sql, con))
                    {
                        con.Open();
                        var version = cmd.ExecuteScalar().ToString();
                        Console.WriteLine($"PostgreSQL version: {version}");
                    }
                }
            }
            catch (NpgsqlException e)
            {
                Console.WriteLine(e.ToString());
            }
            Console.WriteLine("PostgreSQL Done read data.");

            return "END".ToString();
        }
    }
}
